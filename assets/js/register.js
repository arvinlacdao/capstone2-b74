let registerForm = document.querySelector('#registerUser');

registerForm.addEventListener("submit", (e) => {
	// prevents the form to revert to it's default/blank values
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		// fetch('url', {options})
		// to process a request
		
		fetch('http://localhost:4000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail
				// email="arvin@gmail"
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === false){
				
				fetch('http://localhost:4000/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						email: userEmail,
						password : password1,
						mobileNo : mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					if(data === true){
						alert("registered successfully");
						window.location.replace("login.html");
					} else {
						alert("something went wrong");
					}

				})

			} else {
				alert("Email has been used. Try a different")
			}

		})

	} else {
		alert("something went wrong")
	}
})

// event {
// 	property1: values
// }