let navItems = document.querySelector("#navSession");

// localStorage = an object used to store information in our clients/devices
let userToken = localStorage.getItem("token");
console.log(userToken);

// localStorage {
// 	getItem : function () => {
// 		prints key
// 	}
// }

if(!userToken) {
	navItems.innerHTML =
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link">
					Login
				</a>
			</li>
		`
} else {
	navItems.innerHTML =
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link">
					Logout
				</a>
			</li>
		`
}