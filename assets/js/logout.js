// clear the local storage
localStorage.clear();

// redirect to the login page
window.location.replace('login.html');